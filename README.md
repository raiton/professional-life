# Professional Life

# Intro

Since the company I worked in has it's own private repository, i'll guide you through a very simplistic list of my contributions


# NOS

While working as an Accenture consultant, I worked at NOS, one of the biggest telecommunications companies in Portugal. I was put on a front-end 6 people team, responsible to maintain and update said front-end. This front-end was used by most of NOS vendors as a software to manipulate the clients' accounts, including managing portfolios, filing complaints, managing debt, etc...  
The project relied on Java for most of its Logic and ran on Apache Tomcat. The graphical interface was mostly created with JSPs, ICEfaces, HTML & CSS. Some very minor details were solved with JavaScript
Even though the project focused a lot on maintenance (either because something was not working, or because the business decided to change something) I was lucky enough to be involved in multiple "from scratch" projects, where I'd have to create new interfaces with \*mostly\* JSPs/ICEfaces/CSS, new logic with JAVA, and a new flow for the processes with JPDLs

After 8 months I was transferred to what would be a new middleware CI. This Second CI would be only responsible for handling the ShoppingCart. It would receive requests to "get, modify and verify the shopping cart" from all of the company's front'ends, including a mobile app. It would then take this requests and map them to the multiple required backends (such as Siebel or "rules") to give a prompt and correct response to the front-end, removing all of the logic from the front-ends

**Technology**
*  JAVA
*  SOAP requests
*  WSDLs
*  JBPM (JPDLs)
*  JSP
*  CSS
*  HTML with Icefaces
*  SQL 
*  XML
*  JavaScript (Super rarely used)
*  GIT (Gitlab & Tortoise Git)
*  Harvest
*  Jenkins
*  Jira
*  Katalon
*  Waterfall methodology

**What I Learned**
* How to work in an environment with hundreds of people
* How to communicate with douzens of teams while developing projects
* Improved my JAVA abilitys by a fair amount
* How to build pages with JSPs
* How to build flows with JPDLs
* How to use/read service requests to get/send information from/to other teams (like siebel, tibco, etc) (we did have an integration team that would translate the requests)
* Architecture of Software (unfortunatly my college degree didn't touch this topic. Luckly I was able to study a bit the architecture of the front-end project I was in.)


# College

College was a great experience in my life. I had the opportunity to balance my lifestile and passions (like investing), with computer science. Even though it wasn't my main focus at the time I did fairly good when compared to colleagues who focused on it full-time. I really enjoyed learning about security (19/20) and calculus (16/20), and found out how boring I find AI and related topics (10/20) 

Some of the technologies I had the opportunity to learn in college include:
* MongoDB
* Express
* Angular
* Node.js
* HTML
* CSS
* Java (Including crypto library)
* JSP
* C
* SQL
* Haskell
* Assembly
* Blender
* Prolog
* RDF(S)
* SPARQL
* OWL(2)
* SWRL